import { Injectable, NgModule } from '@angular/core';
import { Http } from '@angular/http';
import { Routes } from '@angular/router';
import 'rxjs/add/operator/toPromise';

import { environment } from '../environments/environment';

/*
 * Wordpress API
 * Need to find a better way of getting all pages, categories and posts
 * Maybe use the sitemap functionality which should list all pages in the site
 */

@Injectable()
export class AppRoutingService {
  public routes: Routes = [];

  constructor(
    private http: Http
  ) { }

  addRoutes(items) {
    items.forEach(route => {
      let path = route.link.slice(environment.url.length + 1, -1);
      let type = route.type ? route.type : route.taxonomy;
      if (path.startsWith('./')) {
        path = path.slice(2);
      }
      this.routes.push({
          pathMatch: 'full',
          path: path,
          loadChildren: './' + type +
                        '/' + type +
                        '.module#' + type.charAt(0).toUpperCase() + type.slice(1) +
                        'Module',
          data: {
            title: route.title ? route.title.rendered : route.name,
            type: type
          }
      });
    });
  }

  getRoutes() {
    return new Promise((resolve, reject) => {
      this.http.get(environment.url + '/wp-json/wp/v2/pages')
        .toPromise()
        .then(res => {
          this.addRoutes(res.json());
          this.http.get(environment.url + '/wp-json/wp/v2/categories')
            .toPromise()
            .then(res => {
              this.addRoutes(res.json());
              this.http.get(environment.url + '/wp-json/wp/v2/posts')
                .toPromise()
                .then(res => {
                  this.addRoutes(res.json());
                  resolve(this.routes);
                }, reject);
            }, reject);
        }, reject);
    });
  }
}

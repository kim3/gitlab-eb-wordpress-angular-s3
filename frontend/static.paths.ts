const request = require('request');

import { environment } from './src/environments/environment';

console.log('ENV', environment.url);

const routes = ['/'];

/*
 * Wordpress API
 * Need to find a better way of getting all pages, categories and posts
 * Maybe use the sitemap functionality which should list all pages in the site
 */

function addPaths(items) {
  items.forEach(item => {
    let path = item.link.slice(environment.url.length + 1, -1);
    if (path.startsWith('./')) {
      path = path.slice(2);
    }
    routes.push('/' + path);
  });
}

export function getPaths() {
  return new Promise(function (resolve, reject) {
    request(environment.url + '/wp-json/wp/v2/pages', { json: true }, (err, res, pages) => {
      if (err) return reject(err);
      addPaths(pages);
      request(environment.url + '/wp-json/wp/v2/categories', { json: true }, (err, res, categories) => {
        if (err) return reject(err);
        addPaths(categories);
        request(environment.url + '/wp-json/wp/v2/posts', { json: true }, (err, res, posts) => {
          if (err) return reject(err);
          addPaths(posts);
          resolve(routes);
        });
      });
    });
  })
}

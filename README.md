# angular-universal-wordpress-cms

Example app using the following libraries:
* Angular Universal
* Wordpress CMS


## Installation

If you need to use an existing database, place it at:

    backend/backup.sql

Start by running the docker container locally:

    docker-compose up

Then go to the admin to continue installation at:

    http://localhost:8080/admin

If using the example database you can log in using:

    u: admin
    p: password

Enable permalinks by going to:

    http://localhost:8080/wp-admin/options-permalink.php

Setting Custom Structure to be:

    /%category%/%postname%/

Setting Category Base to be a fullstop:

    .


## Usage

View the Angular frontend at:

    http://localhost:4200/

And use the Wordpress API at:

    http://localhost:8080/wp-json/wp/v2/posts


## Running Code Quality locally

Check backend php code:

    find backend -name "*.php" -print0 | xargs -0 -n1 -P8 php -l

Check frontend JavaScript code:

    cd frontend
    npm install tslint eslint
    npm run lint


## Running tests locally

Install the qa dependencies locally:

    cd qa
    make qa_install
    source env/bin/activate
    export PYTHONPATH=$PWD/env/lib:$PYTHONPATH

Unit, functional, Performance and Visuals tests respectively:

    phpunit tests
    npm test
    behave qa/functional/features
    locust --clients=2 --hatch-rate=1 --num-request=4 --no-web -f qa/performance/locustfile.py --host=http://localhost:8080
    EYES_API_KEY=XXX behave qa/visual/features

Run Zap tests:

    BASE_URL=http://localhost:8080 ZAP_ADDRESS=0.0.0.0:8081 ZAP_API_KEY=0123456789 python qa/security/zap_scanner.py
    behave qa/security/features

Run Accessibility tests:

    BASE_URL=http://172.19.0.1:8080 python qa/accessibility/page_runner.py
    behave qa/accessibility/features

Run Analytics tests:

    behave qa/analytics/features


## Automatic Deployment

Check the .gitlab-ci.yml to see the steps. Set Gitlab Environment Variables as:

    `AWS_ACCESS_KEY_ID`: `XX`
    `AWS_DEFAULT_OUTPUT`: `json`
    `AWS_DEFAULT_REGION`: `us-east-1`
    `AWS_SECRET_ACCESS_KEY`: `XX`
    `PROJECT_NAME`: `wordpress-cms`
    `PROJECT_URL`: `https://www.example.com`

Optional QA variables:

    `ADMIN_EMAIL`: `admin@gmail.com`
    `ADMIN_PASSWORD`: `XX`
    `EDITOR_EMAIL`: `editor@gmail.com`
    `EYES_API_KEY`: `XX`
    `RECOVERY_EMAIL`: `recovery@gmail.com`
    `USER_EMAIL`: `user@gmail.com`
    `USER_PASSWORD`: `XX`
    `ZAP_API_KEY`: `XX`


## Manual Deployment (Backend API)

Install the Elastic Beanstalk client:

    pip install awsebcli --upgrade --user

Create an environment:

    cd backend
    eb init --profile home
    eb init wordpress-cms --region us-east-1
    eb create master --cname master-wordpress-cms --database --single

Go to the admin panel:

    eb console
    Configuration > Software Configuration

Set the backend environment variables:

    WORDPRESS_DB_HOST: XX.us-east-1.rds.amazonaws.com:3306
    WORDPRESS_DB_USER: XX
    WORDPRESS_DB_PASSWORD: XX
    WORDPRESS_DB_NAME: wordpress
    GITLAB_TRIGGER_ID: 0123456
    GITLAB_TRIGGER_TOKEN: abcdefg

Deploy a new version:

    eb deploy


## Manual Deployment (Frontend Static)

Install the AWS Client:

    pip install awscli awsebcli --upgrade --user

Ensure you have just the backend and mysql services running:

    docker-compose stop
    docker-compose up backend mysql

In a second terminal window, generate the static frontend build:

    cd frontend
    npm install
    npm run build:prerender

Or build with custom base url:

    ng build --prod --base-href http://angular-universal-example.s3-website-us-east-1.amazonaws.com
    ng build --prod --app 1 --output-hashing=false --base-href http://angular-universal-example.s3-website-us-east-1.amazonaws.com
    npm run webpack:server && npm run generate:prerender

To view the statically generated version locally use:

    npm run serve:prerender


## Exporting the database

Export database to local:

    docker-compose exec mysql /usr/bin/mysqldump -u root --password=example --databases wordpress > backend/backup.sql


## Directory structure

    frontend/                               --> Frontend sources files
    frontend/static.paths.ts                --> Static generation for Wordpress API
    frontend/app/app-routing.server.ts      --> Angular routing for Wordpress API


## Contact

For more information please contact kmturley

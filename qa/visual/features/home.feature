Feature: The about page's style doesn't accidentally change

@browser
Scenario:
  Given I am on "/"
    And I start "Home Page" of "localhost" at "tablet"
  When the "Home Page" should look as expected
    And I type in "Example Change"
  Then the "With text in Search Field Home Page" should look as expected
    and we close eyes

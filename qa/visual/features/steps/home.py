'''
Feature: Behave's Predefined Data Types in parse page

  @browser
  Scenario:
    Given I am on "/"
      And I start "Home Page" of "localhost" at "tablet"
    When the "Home Page" should look as expected
      And I type in "Example Change"
    Then the "With text in Search Field Home Page" should look as expected
      and we close eyes

'''
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from behave import given, when, then
from qa.environment_variables import BASE_URL, DRIVER, SELENIUM, SL_DC
from qa.functional.features.browser import Browser
import time

# Locator Map
SEARCH_FIELD_LOCATOR = (By.CSS_SELECTOR, 'input[name="search"]')


@when('I type in "{search_term}"')
def click_a_button(context, search_term):
    search_field = context.driver.find_element(*SEARCH_FIELD_LOCATOR)
    search_field.clear()
    search_field.send_keys(search_term)

* [Behave](qa/functional) (Unit, End-to-End, and [Analytics](qa/Analytics) tests)
* [Applitools](qa/visual) (Visual Regression Testing)
* [Locust](qa/performance) (Performance tests)
* [Lighthouse](qa/accessibility) (Accessibility & Mobile Support)
* [Zap](qa/security) (Penetration / Security Tests)
[\*](#caveats)



## Install
##### Dependancies
Install [python 3](https://www.python.org/downloads/) and [Docker](https://store.docker.com/editions/community/docker-ce-desktop-mac) using their .dmg files. Written at Python 3.6.1 for OSX.
Install virtualenv if not already installed.
```
pip install virtualenv
```
##### Install steps
```
make qa_install
```

Edit the file qa/environment_variables.py to match your development setup(localhost, BASE_URL, Selenium Server, etc), if necessary.

##### Setting Local Environment Variables
Copy the following text and add it to the end of ```qa/env/bin/activate```, then edit in your credentials so you no won't have to add them on the command line when running tests locally.
```
export GOOGLE_API_KEY='0123456789'
export EYES_API_KEY='0123456789'
export ZAP_ADDRESS='0.0.0.0:8081'
export ZAP_API_KEY='0123456789'

export ADMIN_EMAIL='fakeuser1@gmail.com'
export ADMIN_PASSWORD='fakepassword'
export ADMIN_NAME='Al Admin'

export EDITOR_EMAIL='fakeUser2@gmail.com'
export EDITOR_PASSWORD='fakepassword'
export EDITOR_NAME='Eddie Editor'

export USER_EMAIL='fakeUser3@gmail.com'
export USER_PASSWORD='fakepassword'
export USER_NAME='Vinny Testaverde'

```
Add the EYES_API_KEY, ZAP_ADDRESS, ZAP_API_KEY, & GOOGLE_API_KEY to the secrets file of the project in gitlab, found by selecting the project then selecting Settings in Project bar and then Pipelines in the sub bar (e.g https:gitlab-address/<user>/<project>/settings/ci_cd). ZAP_ADDRESS & ZAP_API_KEY are set in qa/security/zap_scanner.py.


## Running Tests
Instructions for running tests can be found in their individual README.md files.
* [Behave](qa/functional#running-tests)
* [Analytics](qa/analytics)
* [Applitools](qa/visual)
* [Locust](qa/performance#running-tests)
* [Lighthouse](qa/accessibility#running-tests)
* [Zap](qa/security#running-tests)

#### Run All tests

In one terminal window run
```
make zap_serve
```

In another run the following command (BASE_URL and ZAP_SERVER_PROXY optional, but will let this run without an accounts.py file or or something to test against on localhost)
```
BASE_URL=https://example.com make test_all
```

# Accessibility

## Introduction

Test written using [Behave Framework](http://pythonhosted.org/behave/) and [Lighthouse](https://github.com/GoogleChrome/lighthouse)


## Install
*(if you didn't use main setup.sh script)*
Install [Docker](https://store.docker.com/editions/community/docker-ce-desktop-mac) if not already installed.
```
docker pull kmturley/lighthouse-ci
```
Create a virtualenv if not already.
```
virtualenv -p python2.7 qa/env
```
Install dependencies to virtualenv.
```
source qa/env/bin/activate
pip install -r qa/accessibility/requirements.txt
```

# Running Tests
This command will run against all pages the index page and all pages in PAGES_LIST from environment_variables.py. BASE_URL is optional but without it it will run locally. See below for individual run commands.
```
source qa/env/bin/activate
BASE_URL=http://172.19.0.1:3000 python qa/accessibility/page_runner.py
```
Note: On mac you can not hit http://localhost:3000 from inside another docker container. If you need to get URL for your machine run ```docker ps``` to get list of running docker servers, local the name of the image, then use ```docker inspect NAME``` replacing 'NAME' with the name of the backend image. Under the 'NetworkSettings > Networks > Gateway' there's an address that should work.


To run lighthouse report generator. These will generate reports based off the end of the path. So ```--output-path=/lighthouse/output/about``` will create a report at ```accessibility/output/about.report.json```
```
PAGE=/about python qa/accessibility/single_run.py
```

Then run behave assertions against them, note example needs to match the name used for the end of the output path in the command below.
```
FILE_NAME=about behave qa/accessibility/features
```

If you need a more human readable file fun.
```
FORMAT=html python qa/accessibility/single_run.py
```

###### Dependancies

The PAGES_LIST in qa/environment_variables contains valid URLs for the domain.

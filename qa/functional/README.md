# End-to-End

## Introduction

Test written using [Behave Framework](http://pythonhosted.org/behave/)

## Install
*(if you didn't use main setup.sh script)*
Create a virtualenv if not already.
```
virtualenv -p python2.7 qa/env
```
Install dependencies to virtualenv.
```
source env/bin/activate
pip3 install -r qa/functional/requirements.txt
curl -L https://github.com/mozilla/geckodriver/releases/download/v0.17.0/geckodriver-v0.17.0-macos.tar.gz | tar xz -C qa/env/bin
curl -L https://chromedriver.storage.googleapis.com/2.34/chromedriver_mac64.zip | tar xz -C qa/env/bin
```
* pip install chromedriver_installer==0.0.6 not working in python 3.6 due to certificate issue

#### Safari setup
To test in Safari you must turn on automation in the dev menu, (Develop > Allow Remote Automation) and directly run webdriver once to authorize permissions (In Terminal: /usr/bin/safaridriver -p 8000).

## Running Tests
Be sure to source virtualenv (```source qa/env/bin/activate```) before running tests.

#### Run all tests.

```
behave qa/functional/features
```

#### Changing domain or browser
The Driver default, base url, and other variables are being defaulted in the environment_variables.py but can be overwritten on the command line.
```
DRIVER='chrome' BASE_URL='http://example.com' behave qa/functional/features
```

#### Running Single files or tests
Python breaks things up by **features**, for example the News and Ideas page filters could be a feature. It might have several user stories or as Behave calls them **scenarios**, for example a scenario about the news and ideas page might be "When you click an item it filters the list" or simply "Icons appear for all filters".
You can include or exclude tests with the ```--include``` or ```--exclude``` flags that use feature file names.
```
behave qa/functional/features -i google -e example
```
Or run a single scenario from a feature with the ```--name``` flag:
```
behave qa/functional/features -n 'This is a scenario name'
```

And this should work for Sauce Labs (Note that the parenthesis on SL_DC on mandatory on this command when they're optional for the rest of example variables.) You of course have to change the url where is says *YOUR_SAUCE_USERNAME* and *YOUR_SAUCE_ACCESS_KEY* to credentials:
```
SELENIUM=http://YOUR_SAUCE_USERNAME:YOUR_SAUCE_ACCESS_KEY@ondemand.saucelabs.com:80/wd/hub SL_DC='{"platform": "Mac OS X 10.9", "browserName": "chrome", "version": "31"}'  DRIVER=saucelabs BASE_URL=https://example.com behave qa/functional/features
```
\* haven't tried sauce yet.

### Notes about example tests.

* To stop getting Python Permission prompts on Chromedriver launch follow these [instructions](http://bd808.com/blog/2013/10/21/creating-a-self-signed-code-certificate-for-xcode/) to create a certificate. Run `. env/bin/activate` once and then stop it to get in the virtual env. It should now say `(env)` on Terminal command line, then run this command, replacing *NAME* with the name of the certificate you created.
```
codesign -s NAME -f `which python`
```

## Viewing tests using VNC

Install a VNC Viewer such as:
https://chrome.google.com/webstore/detail/vnc%C2%AE-viewer-for-google-ch/iabmpiboiopbgfabjmgeedhcmjenhbla?hl=en

Then connect using:
```
Address: localhost:5900
Password: secret
```

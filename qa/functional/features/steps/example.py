'''
@browser
Feature: Our Site has expected text

  Scenario: There should be a title
    Given I am on "/"
    Then the page should have the correct title

'''
import sys
import time
import unittest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from behave import given, when, then
from hamcrest import assert_that, contains_string, equal_to
import time


# Locator Map
# SEARCH_FIELD_LOCATOR = (By.ID, 'search-input')

# Note "I am on {page}" defined in qa/functional/features/steps/common.py


# @given('there is a search field and it is empty')
# def locate_search_field(context):
#     context.search_field = context.driver.find_element(*SEARCH_FIELD_LOCATOR)
#     context.search_field.clear()


@when('I send the text "{search_term}" to the field')
def send_test_to_search_field(context, search_term):
    context.search_field.send_keys('video')
    sys.stdout.write('No hitting Return since field not hooked up')
    time.sleep(2)
    # context.search_field.send_keys(Keys.RETURN)


@then('the page should have the correct title')
def assert_page_title(context):
    assert context.driver.title == 'Gitlab EB Wordpress Angular S3', \
        'Did not get expected title, instead:\n%s' % (context.driver.title)


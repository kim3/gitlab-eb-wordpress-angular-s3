Feature: Does not reflect XSS

  @browser
  Scenario: If we enter an xss attack it should not work on another page
  Given I am on "/"
  When I set current element to search field
    and I send the attack "\"><h1>XSS</h1><\""
  Then there should not be an element with this locator "h1#XSS"

import os

BASE_URL = os.getenv('BASE_URL', 'http://localhost:4200')
DRIVER = os.getenv('DRIVER', 'chrome')
DRIVER = DRIVER.lower().replace(' ', '_').replace('-', '_')
LIGHTHOUSE_IMAGE = os.getenv('LIGHTHOUSE_IMAGE', 'http://localhost:8085')
SELENIUM = os.getenv('SELENIUM', 'http://localhost:4444/wd/hub')
SL_DC = os.getenv(
    'SL_DC',
    '{"platform": "Mac OS X 10.12.6", "browserName": "chrome", "version": "63"}'
)
# Add all urls except the index to this list
PAGES_LIST = []
QA_FOLDER_PATH = os.getenv('QA_FOLDER_PATH', 'qa/')
ADMIN_URL_DICT = {
    'https://www.example.com': 'https://www.example.com/admin-uri',
    'https://www.testing.com': 'https://www.testing.com/admin-uri',
    'https://www.dev.com': 'https://www.dev.com/admin-uri',
    'https://www.staging.com': 'https://www.staging.com/admin-uri'
}
ADMIN_EMAIL = os.getenv('ADMIN_EMAIL', 'test11@gmail.com')
ADMIN_PASSWORD = os.getenv('ADMIN_PASSWORD', 'fakepassword')
ADMIN_NAME = os.getenv('ADMIN_NAME', 'Al\' Admin')

EDITOR_EMAIL = os.getenv('EDITOR_EMAIL', 'fakeUser2@gmail.com')
EDITOR_PASSWORD = os.getenv('EDITOR_PASSWORD', 'fakepassword')
EDITOR_NAME = os.getenv('EDITOR_NAME', 'Eddie Editor')

USER_EMAIL = os.getenv('USER_EMAIL', 'fakeUser3@gmail.com')
USER_PASSWORD = os.getenv('USER_PASSWORD', 'fakepassword')
USER_NAME = os.getenv('USER_NAME', 'Vinny Testaverde')

ZAP_ADDRESS = os.getenv('ZAP_ADDRESS', 'http://localhost:4200')
ZAP_API_KEY = os.getenv('ZAP_API_KEY', '0123456789')

EYES_API_KEY = os.getenv('EYES_API_KEY', '0123456789')

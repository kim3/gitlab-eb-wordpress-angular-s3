Feature: Our page's analytics always work!

  Scenario: The Homepage fires an event when it loads
  Given I am on "/"
  When I check logs
  Then I should see "title" with a value of "Gitlab EB Wordpress Angular S3"

  Scenario: When I click a navigation item it should fire correct events
    Given I am on "/"
    When I click Outbound link example
      And I check logs
    Then I should see "eventLabel" with a value of "https://www.google.com/"
      And I should see "eventCategory" with a value of "Outbound Link"
      And I should see "eventAction" with a value of "click"

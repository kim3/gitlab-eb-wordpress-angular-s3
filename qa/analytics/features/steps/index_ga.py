'''
  Scenario: The Homepage fires an event when it loads
  Given I am on "/"
  When I check logs
  Then I should see "title" with a value of "Gitlab EB Wordpress Angular S3"

  Scenario: When I click a navigation item it should fire correct events
    Given I am on "/"
    When I click Outbound link example
      And I check logs
    Then I should see "eventLabel" with a value of "label"
      And I should see "eventCategory" with a value of "link"
      And I should see "eventAction" with a value of "click"

'''
import time
from behave import given, when, then
from hamcrest import assert_that, contains_string, equal_to
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from qa.analytics.features.steps.common import CommonFunctions
from qa.environment_variables import BASE_URL, DRIVER, SELENIUM, SL_DC, QA_FOLDER_PATH

OUTBOUND_LINK_SELECTOR = (By.CSS_SELECTOR, '.outbound')


@when('I click Outbound link example')
def click_navigation(context):
    get_started_button = context.driver.find_element(*OUTBOUND_LINK_SELECTOR)
    get_started_button.click()
